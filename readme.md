# Frequently asked Questions #

![4.9.3](https://img.shields.io/badge/version-4.9.3-green.svg)

Модуль позволяет добавить FAQ 


### Details ###

Данный модуль добавит новый пункт меню - Website -> FAQ, на котором будет доступен список вопросов.
В новом меню можно создавать / редактировать / удалять / просматривать вопросы.

* Developed for CS-CART EDITION 4.9.3

### What should I check? ###

* Проверь  Website -> FAQ

### Questions? ###

* Developer: Kutyumov Andrey
* Manager: Sergei Minyukevich